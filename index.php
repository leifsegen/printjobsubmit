<?php
require_once('fpdf/fpdf.php');
require_once('fpdi/fpdi.php');

/*
Get from user:
schoolName
phoneNumber
contactPerson
jobName
numPages (original)
numCopies
color (boolean)
doubleSided (boolean)
	if doubleSided:
		flipAlong (longSide or shortSide)
paperSize (choose 8.5xll, 11x17, 8.5x14)
staple (upperleft only)
collate (boolean) [show 123 123 123 vs 111 222 333]
threeHolePunch (boolean)
deliveryMethod (districtMail or willPickUp)
	if willPickUp:
		phoneNumber (prepopulate)
		name (prepopulate)

Autoget:
Date of submission
 */

$pdf = new FPDI("P","in","Letter");

$pageCount = $pdf->setSourceFile("PrintShopJobTicketOriginal.pdf");
$tplIdx = $pdf->importPage(1, '/MediaBox');

$pdf->addPage();
$pdf->useTemplate($tplIdx, 0, 0, 8.5);

$pdf->SetFont('Arial','',12);
$pdf->SetX(5.2);
$pdf->SetY(1.7);
$pdf->Cell(0,0,'Alex Nef');
$pdf->SetX(12);
$pdf->SetY(2.6);


$pdf->Output();